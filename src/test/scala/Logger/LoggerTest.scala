package Logger

import DataBase.DataBase
import HMDataBase.HMDataBase
import Models.{Message, MessageStatus}
import com.bot4s.telegram.models.{Chat, ChatType, User, Message => Msg}
import org.scalatest.{AsyncFlatSpec, Matchers}

abstract class LoggerTest(dataBase: DataBase) extends AsyncFlatSpec with Matchers {
  val logger = new Logger(dataBase)

  behavior of "logReceivedMessage"

  val message = Msg(1, from = Some(User(1, isBot = true, "Tester")), date = 1, Chat(1, ChatType.Private), text = Some("Test"))
  "receivedMessage".should(right = "be added in DB").in {
    logger.logReceivedMessage(message)
    dataBase.messages.get(1,1).map({
      case Some(Message(_, userId, chatId, messageId, date, text, status)) =>
        userId shouldBe 1
        chatId shouldBe 1
        messageId shouldBe 1
        date shouldBe 1
        text shouldBe "Test"
        status shouldBe MessageStatus.Alive
    })
  }

  behavior of "logChangedMessage"

  "message status and text".should(right = "be changed").in {
    val message = Msg(2, from = Some(User(1, isBot = true, "Tester")), date = 1, Chat(1, ChatType.Private), text = Some("Test"))
    val changedMessage = message.copy(text = Some("Test2"))
    for {
      _ <-logger.logReceivedMessage(message)
      _ <- logger.logChangedMessage(changedMessage)
      result <- dataBase.messages.get(1, 2).map({
        case Some(Message(_, userId, chatId, messageId, date, text, status)) =>
          userId shouldBe 1
          chatId shouldBe 2
          messageId shouldBe 1
          text shouldBe "Test2"
          status shouldBe MessageStatus.Changed
      })
    } yield result

  }
}

class HMDataBaseMessageLoggerTest extends LoggerTest(new HMDataBase())

