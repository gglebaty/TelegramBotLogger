package HMDataBase

import com.bot4s.telegram.models.User
import org.scalatest.{AsyncFlatSpec, Matchers}

class UsersHMDataBaseTest extends AsyncFlatSpec with Matchers{
  val hmDataBase = new UsersHMDataBase()

  val user = User(1, isBot = false, "Test")
  "user" should "be added in users" in{
    hmDataBase.insert(user)
    hmDataBase.users should contain(user.id -> user)
  }

  "method get" should "return right user by id" in{
    hmDataBase.get(Seq(1)).map(_ shouldBe List(user))
  }

}
