package HMDataBase

import Models.Chat
import com.bot4s.telegram.models.ChatType
import org.scalatest.{AsyncFlatSpec, Matchers}

class ChatsHMDataBaseTest extends AsyncFlatSpec with Matchers{

  val hmDataBase = new ChatsHMDataBase()

  val chat: Chat = Chat(1, ChatType.Private)
  "chat" should "be added in chats" in{
    hmDataBase.insert(chat)
    hmDataBase.chats should contain(chat.id -> chat)
  }

  "method getAll" should "return right list of chats" in{
    hmDataBase.getAll.map(_ shouldBe Seq(chat))
  }

  "method getById" should "return right chat by id" in{
    hmDataBase.getById(1).map(_ shouldBe Some(chat))
  }

}
