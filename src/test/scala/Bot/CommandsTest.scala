package Bot


import java.util.UUID

import HMDataBase.HMDataBase
import Models.{Change, Chat, Message, MessageStatus}
import com.bot4s.telegram.models.{ChatType, User}
import org.scalatest.{AsyncFlatSpec, Matchers}


class CommandsTest extends AsyncFlatSpec with Matchers {

  val database = new HMDataBase
  val commands = new Commands(database)

  "formattedDate" should "return date in right format" in {
    commands.formattedDate(0) shouldBe "01.01.1970_03.00"
  }

  val description = "LoggerBot is the bot, which collect all the information you write to it"

  "botCommand" should "return description on command '/description'" in {
    commands.botCommands(1, "/description").map(
      _ shouldBe description
    )
  }

  "description" should "return right description" in {
    commands.description().map(_ shouldBe description)
  }

  "start" should "return right start phrase" in {
    commands.start().map(_ shouldBe "From now, everything you say plays against you")
  }

  val chat = Chat(1, ChatType.Private)
  database.chats.insert(chat)
  "chats" should "return right seq of chats" in {
    commands.chats().map(_ shouldBe Seq(chat))
  }

  val uuid: UUID = UUID.randomUUID()
  val message = Message(uuid, 1, 1, 1, 1, "Test", MessageStatus.Alive)
  database.messages.insert(message)
  "messages" should "return right seq of messages" in {
    commands.messages("/messages user=1", 1.toLong).map(_ shouldBe Seq(message))
  }

  "messagesAsString" should "return right message" in {
    commands.messagesAsString("/messages user=1", 1.toLong).map(_ shouldBe "01.01.1970_03.00\nTest")
  }

  val change = Change(uuid, 1, "Test2")
  database.changes.insert(change)
  "changes" should "return right seq of messages and their changes" in {
    commands.changes("/changes user=1", 1).map(_ shouldBe Seq((message, Seq(change))))
  }

  "changesAsString" should "return right messages" in {
    commands.changesAsString("/changes user=1", 1).map(
      _ shouldBe s"Message:\n  |01.01.1970_03.00\n  |Test\nChanges:\n  |01.01.1970_03.00\n  | Test2"
    )
  }

  val user = User(1, isBot = false, "Test")
  database.users.insert(user)
  "users" should "return right seq of users" in {
    commands.users(1).map(
      _ shouldBe Seq(user))
  }

  "usersAsString" should "return right string" in {
    commands.usersAsString(1).map(
      _ shouldBe "User - Test")
  }

  "statistic" should "return right seq" in {
    commands.statistic(1).map(_ shouldBe Seq((user, 1)))
  }

  "statisticAsString" should "return right string" in {
    commands.statisticAsString(1).map(_ shouldBe "User: Test\n  Messages:1")
  }


}
