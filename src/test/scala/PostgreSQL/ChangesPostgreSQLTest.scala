package PostgreSQL

import java.util.UUID

import Models.Change
import org.scalatest.{AsyncFlatSpec, Matchers}
import slick.jdbc.JdbcProfile


abstract class ChangesPostgreSQLTest(profile: JdbcProfile) extends AsyncFlatSpec with Matchers{
    /*import slick.jdbc.H2Profile.api._*/
    val url = "jdbc:h2:mem:test1"
    val driver = "org.h2.Driver"
    val schema = new Schema(url, driver)
    val changes = new ChangesPostgreSQL(schema)

    val uuid: UUID = UUID.randomUUID ()
    val change = Change(uuid, 1, "Test")

    /*"change" should "be added in changes" in{
        changes.insert(change)
        changes.get(uuid).map(_ shouldBe change)
    }*/
}
