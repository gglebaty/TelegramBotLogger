package PostgreSQL

import DataBase.ChatsStore
import Models.Chat
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class ChatsPostgreSQL(schema: Schema)(implicit e: ExecutionContext) extends ChatsStore{
  def insert(chat: Chat): Future[Int] =
    schema.db.run(schema.chats += chat)

  def getById(id: Long): Future[Option[Chat]] =
    schema.db.run(schema.chats.filter(_.id === id).take(1).result.headOption)

  def getAll: Future[Seq[Chat]] =
    schema.db.run(schema.chats.result)

}
