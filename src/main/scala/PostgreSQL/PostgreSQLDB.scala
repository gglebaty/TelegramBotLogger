package PostgreSQL

import DataBase.DataBase

import scala.concurrent.ExecutionContext

class PostgreSQLDB(connectionUrl: String, driver: String)(implicit e: ExecutionContext) extends DataBase{

  private val schema = new Schema(connectionUrl, driver)
  schema.setup()

  val changes = new ChangesPostgreSQL(schema)
  val chats = new ChatsPostgreSQL(schema)
  val messages = new MessagesPostgreSQL(schema)
  val users = new UsersPostgreSQL(schema)
}
