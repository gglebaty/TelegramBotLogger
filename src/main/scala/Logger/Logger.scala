package Logger

import java.util.UUID

import DataBase.DataBase
import Models.{Change, Chat, MessageStatus, Message => Msg}
import com.bot4s.telegram.models.{Message, User, Chat => Cht}

import scala.concurrent.{ExecutionContext, Future}

class Logger(dataBase: DataBase)(implicit ec: ExecutionContext) {

  def currentTime: Int = {
    (System.currentTimeMillis() / 1000).toInt
  }

  def convertToChat(chat: Cht): Chat = {
    Chat(chat.id,
      chat.`type`,
      chat.title,
      chat.username,
      chat.firstName,
      chat.lastName,
      chat.allMembersAreAdministrators,
      chat.description,
      chat.inviteLink)
  }

  def getTextAndUser(message: Message): Option[(User, String)] =
    message.from match {
      case Some(user) =>
        message.text match {
          case Some(text) => Some(user, text)
          case None => None
        }
      case None => None
    }

  def logReceivedMessage(message: Message): Future[Int] = {
    getTextAndUser(message) match {
      case Some((user, text)) =>
        val chat = convertToChat(message.chat)
        val uuid: UUID = UUID.randomUUID()
        val msg = Msg(uuid, message.chat.id, message.messageId, user.id, message.date, text, MessageStatus.Alive)
        dataBase.chats.insert(chat)
        dataBase.users.insert(user)
        dataBase.messages.insert(msg)
      case None => Future.successful(0)
    }
  }

  def logChangedMessage(editedMessage: Message): Future[Int] = {
    getTextAndUser(editedMessage) match {
      case Some((_, text)) =>
        val message = dataBase.messages.get(editedMessage.chat.id, editedMessage.messageId)
        message.flatMap({
          case Some(value) =>
            val change = Change(value.key, value.date, value.text)
            val newMessage = value.copy(date = currentTime, text = text, status = MessageStatus.Changed)
            for {
              _ <- dataBase.changes.insert(change)
              result <- dataBase.messages.insert(newMessage)
            } yield result
          case None => Future.successful(0)
        })
      case None => Future.successful(0)
    }
  }

}