package HMDataBase

import java.util.UUID

import DataBase.MessagesStore
import Models.Message

import scala.collection.mutable
import scala.concurrent.Future

class MessagesHMDataBase extends MessagesStore{
  val messages: mutable.HashMap[UUID, Message] = mutable.HashMap.empty

  def insert(message: Message): Future[Int] =
    Future.successful{
      messages += (message.key -> message)
      1
    }

  def get(chatId: Long, messageId: Int): Future[Option[Message]] =
    Future.successful({
      messages.values.find(message => message.chatId == chatId && message.messageId == messageId)
    })

  def getByUUID(keys: Seq[UUID]): Future[Seq[Message]] =
    Future.successful(messages.filter(tuple => keys.contains(tuple._1)).values.toSeq)

  def drop(key: UUID): Future[Int] =
    Future.successful({
      messages.remove(key)
      1
    })

  def getByChatId(chatId: Long): Future[Seq[Message]] =
    Future.successful({
      messages.values.filter(_.chatId == chatId).toSeq
    })

  def forPeriod(chatId: Long, from: Int, until: Int): Future[Seq[Message]] =
    Future.successful({
      messages.values.filter(msg => msg.chatId == chatId && msg.date > from && msg.date < until).toSeq
    })

  def getUserIds(chatId: Long): Future[Seq[Int]] =
    Future.successful({
      messages.values.filter(msg => msg.chatId == chatId).map(msg => msg.userId).toSeq
    })

  def userMessages(chatId: Long, userId: Int): Future[Seq[Message]] =
    Future.successful({
      messages.values.filter(msg => msg.chatId == chatId && msg.userId == userId).toSeq
    })

  def statistic(chatId: Long, users: Seq[Int]): Future[Seq[(Int, Int)]] =
    Future.successful({
      messages.values.filter(msg => users.contains(msg.userId) && msg.chatId == chatId)
        .groupBy(_.userId).map(tuple => (tuple._1, tuple._2.size)).toSeq
    })
}
