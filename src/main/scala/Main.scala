
import Bot.{Commands, TelegramLoggerBot}
import DataBase.DataBase
import Logger.Logger
import PostgreSQL.PostgreSQLDB
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration

object Main {

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem()
    implicit val mat: ActorMaterializer = ActorMaterializer()
    implicit val e: ExecutionContext = system.dispatcher

    val connectionUrl = ConfigFactory.load("application").getString("DBUrl")
    val driver = "org.postgresql.Driver"
    val repository: DataBase = new PostgreSQLDB(connectionUrl, driver)
    val logger = new Logger(repository)
    val commands = new Commands(repository)

    val bindingFuture = Http().bindAndHandle(Router.routes(commands), "localhost", 8080)
    println(s"Server online at http://localhost:8080/")

    val token: String = ConfigFactory.load("application").getString("token")
    val loggerBot = new TelegramLoggerBot(logger, commands, token)
    val eol = loggerBot.run()
    println("Press [ENTER] to shutdown the bot, it may take a few seconds...")
    scala.io.StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind())(ExecutionContext.global) // trigger unbinding from the port
      .onComplete(_ => system.terminate())(ExecutionContext.global) // and shutdown when done


    loggerBot.shutdown()
    Await.result(eol, Duration.Inf)
  }

}
