package Models

import com.bot4s.telegram.models.ChatType.ChatType

case class Chat(
                 id            : Long,
                 `type`        : ChatType,
                 title         : Option[String] = None,
                 username      : Option[String] = None,
                 firstName     : Option[String] = None,
                 lastName      : Option[String] = None,
                 allMembersAreAdministrators : Option[Boolean] = None,
                 description   : Option[String] = None,
                 inviteLink    : Option[String] = None,
               )
