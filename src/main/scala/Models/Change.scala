package Models

import java.util.UUID

case class Change(id: UUID,
                  date: Int,
                  text: String)
