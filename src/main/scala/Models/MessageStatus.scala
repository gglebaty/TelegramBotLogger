package Models

object MessageStatus extends Enumeration {
  type MessageStatus = Value
  val Alive, Changed, Deleted = Value
}
