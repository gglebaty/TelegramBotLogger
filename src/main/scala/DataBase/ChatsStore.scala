package DataBase

import Models.Chat

import scala.concurrent.Future

trait ChatsStore {
  def insert(chat: Chat): Future[Int]
  def getAll:Future[Seq[Chat]]
  def getById(id: Long): Future[Option[Chat]]
}
