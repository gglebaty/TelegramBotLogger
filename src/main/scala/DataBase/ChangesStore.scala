package DataBase

import java.util.UUID

import Models.Change

import scala.concurrent.Future

trait ChangesStore {
  def insert(change: Change): Future[Int]

  def get(id: UUID): Future[Seq[Change]]

  def forPeriod(ids: Seq[UUID], from: Int, until: Int): Future[Seq[Change]]

  def getAllByUUIDs(uuids: Seq[UUID]): Future[Seq[Change]]
}
