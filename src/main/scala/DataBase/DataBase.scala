package DataBase

trait DataBase {
  val changes: ChangesStore
  val chats: ChatsStore
  val messages: MessagesStore
  val users: UsersStore
}