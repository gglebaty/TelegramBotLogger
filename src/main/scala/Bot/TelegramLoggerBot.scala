package Bot

import Logger.Logger
import com.bot4s.telegram.api._
import com.bot4s.telegram.clients.ScalajHttpClient
import com.bot4s.telegram.methods._
import com.bot4s.telegram.models._

import scala.concurrent.Future
import scala.util.{Failure, Success}


class TelegramLoggerBot(messageLogger: Logger, commands: Commands, token: String) extends TelegramBot with Polling {

  val client = new ScalajHttpClient(token)

  def sendMessage(chatId: Long, message: String): Future[Message] = request(SendMessage(chatId, message))

  override def receiveMessage(message: Message): Unit = {
    message.text match {
      case Some(text) =>
        if(text.startsWith("/"))
          commands.botCommands(message.chat.id, text).andThen{
            case Success(msg) => sendMessage(message.chat.id, msg)
            case Failure(_) => sendMessage(message.chat.id, "Something going wrong. Please try later")
          }
        else
          messageLogger.logReceivedMessage(message)
      case _ =>
    }
  }

  override def receiveEditedMessage(editedMessage: Message): Unit = {
    messageLogger.logChangedMessage(editedMessage)
  }

}
