# TelegramBotLogger

This is a repository for [TelegramBotLogger](https://t.me/TFS_2019_LoggerBot)

## Introduction
Telegram Bot-Logger is the bot which collects everything you say to it. From the moment when the bot was added to any chat it starts to remember all the information about text messages which come to the chat, edited messages and chat users.
<br/>Any chat user can access to logged information, but only in the borders of the current chat.

## Bot commands
Commands available in chat:
*	_/description_ – short bot description
*	_/users_ – list of active users of the chat (users who have ever written any text messages in the chat)
*	_/statistic_ – statistic of number of sent messages for every active user in the chat
*	_/changes – list of all_ changes have been made in the chat
*	_/messages_ (get two parameters: start date and end date in format _dd.MM.yyyy_HH.mm_) – list of messages sent for preset period of time
<br/>Commands "/messages" and "/changes" can get list of parameters:
<br/>_user_ - user id whose messages you want to find
<br/>_from_ - start date from which you want to search messages
<br/>_until_ - end date until which you want to search messages

## Server routes
There is individual server for the bot which also give you access to all logged data.
Routes available on the server:
*	_/chats_ – list of chats where bot is one of the users
*	_/chats/Id_ – list all the messages in chat with given id
*	_/chats/Id/users_ – list of all the active users of chat with given id
*	_/chats/Id/changes_ – list of message changes made in chat with given id
*	_/chats/Id/statistic_ – statistic of number of sent messages for every active user in chat with given id
*	_/chats/Id/messages_ – list of messages in chat with given id
<br/>Messages and changes can get the same list of parameters as in the bot

## Diagram
![Diagram](Diagram.png)
## Authors
*   Gleb Vergeychik - _Owner/maintainer_ - [glebbaty](https://gitlab.com/gglebaty)